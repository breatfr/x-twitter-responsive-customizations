# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
[X (twitter)](https://x.com/) website is more suitable for wide screens.
## Preview
![Preview](https://gitlab.com/breatfr/x-twitter/-/raw/main/docs/preview.jpg)

## List of available customisations:
- hide message box at the bottom
- hide premium
- hide sidebar on left
- hide sidebar on right
- center the logo
- preview modes for images and videos:
     - contain + blur around
     - contain
     - cover (with crop)
- old logo
- wide mode

## How to use in few steps
1. Install Stylus browser extension
    - Chromium based browsers link: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [UserStyles.world](https://userstyles.world/style/16249) website and click on `Install` under the preview picture or open the [GitLab version](https://gitlab.com/breatfr/x-twitter/-/raw/main/css/x-twitter-responsive-customizations.user.css).

3. To update the theme, open the `Stylus Management` window and click on `Check for update` and follow the instructions or just wait 24h to automatic update

4. Enjoy :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>